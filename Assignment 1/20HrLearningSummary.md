***Learn Anything in 20 Hours***


- This summary explains the infamous "10,000 hours to learn anything" is in fact ,untrue.
- It takes 10,000 to become an expert in any ultra competitive field,  but to go on from knowing nothing to being pretty good is actually takes 20 hours  
- It takes 10,000 hours to achieve mastery in a field.  But it only takes 20 hours to get good at something, if you practice intelligently.
- If you put 20 hours of focused deliberate practice into that thing,  you will be astounded.  Astounded at how good you are. 
 

![img1](images/WhatsApp_Image_2020-06-25_at_08.58.06__1_.jpeg)

Practicing intelligently is the key to learning any new skill the most effective and efficient way possible.

**Practice Intelligently****

![img](images/WhatsApp_Image_2020-06-25_at_08.58.06__2_.jpeg)

![img](images/WhatsApp_Image_2020-06-25_at_08.58.06__3_.jpeg)

“20 hours isn’t that hard to accumulate.  Now, there’s a method to doing this.  Because it’s not like you can just start fiddling around for about 20 hours and expect these massive 


![img](images/WhatsApp_Image_2020-06-25_at_08.55.56.jpeg)

There’s a way to practice intelligently.  There’s a way to practice efficiently, that will make sure that you invest those 20 hours in the most effective way that you possibly can.”

1-**Deconstruct the Skill**

![img](images/WhatsApp_Image_2020-06-25_at_08.55.56__1_.jpeg)

Break it down.  If you want to learn a new skill, the first thing you need to do is break it down into small action steps.

2-**Learn Enough To Self Correct**

![img](images/WhatsApp_Image_2020-06-25_at_08.55.56__2_.jpeg)

“The second is, learn enough to self correct.  So, get three to five resources about what it is you’re trying to learn.  Could be books, could be DVDs, could be courses, could be anything. 

**3. Remove Practice Barriers****

![img](images/WhatsApp_Image_2020-06-25_at_08.55.56__3_.jpeg)

“The third is to remove barriers to practice.  Distractions, television, internet.  All of these things that get in the way of you actually sitting down and doing the work.

**4. Practice at least 20 hours****

![img](images/WhatsApp_Image_2020-06-25_at_08.55.56__4_.jpeg)

“The major barrier to learn something new is not intellectual, it’s not the process of learning a bunch of little tips or tricks or things.

The major barrier is emotional.  We’re scared.  Feeling stupid doesn’t feel good, in the beginning of learning anything new you feel really stupid.

So the major barrier is not intellectual, it’s emotional.  But put 20 hours into anything.  It doesn’t matter.  What do you want to learn?”



