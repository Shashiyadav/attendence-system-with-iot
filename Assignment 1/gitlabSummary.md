***ABOUT GITLAB***

![](images/WhatsApp_Image_2020-06-27_at_20.23.59__1_.jpeg)

- GitLab is a single application for the entire DevOps lifecycle that allows teams to work together better and bring more value to your customers, faster.
- GitLab is a web-based open source Git repository manager with wiki and issue tracking features and built-in CI/CD.
- GitLab is an open-source code collaboration platform that enables developers to create, review, and deploy code bases. The company's open-source, code-

**INSTALLATION**


- Add the GitLab package repository and install the package Add the GitLab package repository.
- Next, install the GitLab package.
- Change https://gitlab.com to the URL at which you want to access your GitLab instance. 
- Installation will automatically configure and start GitLab at that URL.

**There are some components of gitlab-**


1-***REPOSITORY***

![](images/WhatsApp_Image_2020-06-27_at_20.23.59__2_.jpeg)

- A repository is what you use to store your codebase in GitLab and change it with version control.
- To create a new repository, all you need to do is create a new project or fork an existing project.
- Once you create a new project, you can add new files via UI or via command line
- 

2-***WORKING TREE***

The working tree, or working directory, consists of files that you are currently working on.


***INDEX***

![](images/WhatsApp_Image_2020-06-27_at_20.23.59__3_.jpeg)

-  The git "index" is where you place files you want commit to the git repository.
- The index is a staging area where the new commit is prepared

**STATES OF GIT FILES**

Git views all files in three ways:

![](images/WhatsApp_Image_2020-06-27_at_20.45.39.jpeg)

- committed
- modified/untracked
- staged


***GIT COMMANDS***

![](images/WhatsApp_Image_2020-06-27_at_20.23.59.jpeg)

There are some basic commands of git-

**git config**


- git config that lets you get and set configuration variables that control all aspects of how Git looks and operates

**git init**

- The git init command creates a new Git repository.

**git clone**

- The git clone command copies an existing Git repository.

**git add**

- The git add command adds a change in the working directory to the staging area
- 
- **git commit**


-  The "commit" command is used to save your changes to the local repository.
- Using the "git commit" command only saves a new commit object in the local Git repository.

**git diff**


- Diffing is a function that takes two input data sets and outputs the changes between them

**git reset**


- git reset command is to move the current HEAD to the commit specified (

**git status**


- The git status command displays the state of the working directory and the staging area.

***VOCABULARY***

**Branch**

- Think of your git repo as a tree.
- The trunk of the tree, is called the Master Branch. That’s the one that goes live.
- Branches are separate instances of the code that offshoots from the main codebase.
- 

**Merge**


- Git merging combines sequences of commits into one unified history of commits.

**Clone**

 
- Git clone is a Git command line utility which is used to target an existing repository and create a clone, or copy of the target repository.

**Fork**

 - A fork is a copy of a repository.
-  Forking a repository allows you to freely experiment with changes without affecting the original project. 

 





















