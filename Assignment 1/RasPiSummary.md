**RASPBERRY PI**


- The Raspberry Pi is a series of small single-board computer.


- It is a capable little device that enables people of all ages to explore computing, and to learn how to program in languages like Scratch and Python.

 - Huge processing power in a compact board.
   
- Provide many interfaces (HDMI, multiple USB, Ethernet, onboard Wi-Fi and Bluetooth, many GPIOs, USB powered, etc.)
 


**COMPONENTS**

There are the components of Raspberry pii 

![](images/Raspberry-Pi-Components-Explanation.png)

**USB PORTS**

This is used to connect mouse and keyboard 
The number and type of USB ports on Raspberry Pi depends on the model

![](images/WhatsApp_Image_2020-06-25_at_16.26.42.jpeg)

**DSI Connector**

The Display Serial Interface (DSI) is a specification by the Mobile Industry Processor Interface (MIPI) Alliance aimed at reducing the cost of display controllers in a mobile device. 

![](images/WhatsApp_Image_2020-06-29_at_16.39.01.jpeg)

**SD CARD SLOT**

The Micro SD card slot on the Raspberry Pi is located just below the Display Serial Adapter on the other side. Insert the Micro SD card which was loaded with NOOBS in the slot and plug in power supply.
this is the place where files are stored

![](images/WhatsApp_Image_2020-06-25_at_16.31.15.jpeg)

**ETHERNET PORT**

This is used to connect Raspberry Pi to a network with a cable. Raspberry Pi can also connect to a network via wireless LAN.

![](images/WhatsApp_Image_2020-06-25_at_16.33.37.jpeg)

**AUDIO JACK**

This will play the audio file example.mp3 through either your monitor's built-in speakers or your headphones

![](images/WhatsApp_Image_2020-06-25_at_16.34.39.jpeg)


**HDMI PORT** 

 The Raspberry Pi has an HDMI port which you can connect directly to a monitor or TV with an HDMI cable.
 
 
![](images/WhatsApp_Image_2020-06-25_at_16.43.39.jpeg)

**MICRO USB power connector**

 Micro-usb connector is used to supply power to Raspberry pi 
 
![](images/WhatsApp_Image_2020-06-25_at_16.43.39.jpeg)

**GPIO ports**

A GPIO (general-purpose input/output) port handles both incoming and outgoing digital signals. As an input port, it can be used to communicate to the CPU the ON/OFF signals received from switches, or the digital readings received from sensors.


![](images/WhatsApp_Image_2020-06-25_at_16.43.39__1_.jpeg)

